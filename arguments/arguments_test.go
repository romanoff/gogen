package arguments

import (
	"reflect"
	"testing"
)

func TestIsNamedArgument(t *testing.T) {
	if isNamedArgument("new") == true {
		t.Error("Expected new to be non named argument, but got true")
	}
	if isNamedArgument("-param") == false {
		t.Error("Expected -param to be named argument, but got false")
	}
	if isNamedArgument("--param") == false {
		t.Error("Expected --param to be named argument, but got false")
	}
}

func TestGetNamedArgument(t *testing.T) {
	attr, err := getNamedArgument("--param")
	if err != nil || attr != "param" {
		t.Errorf("Expected to get param as named argument, but got %v, error - %v", attr, err)
	}
	attr, err = getNamedArgument("-param")
	if err != nil || attr != "param" {
		t.Errorf("Expected to get param as named argument, but got %v, error - %v", attr, err)
	}
	attr, err = getNamedArgument("param")
	if err == nil || attr != "" {
		t.Errorf("Expected to get error for 'param' named argument, but got %v, error - %v", attr, err)
	}
	if err.Error() != "Not a named argument" {
		t.Errorf("Expected to get 'Not a named argument' error, but got '%v'", err.Error())
	}

}

func TestParseArguments(t *testing.T) {
	arguments := []string{"big", "red"}
	inline, named, err := ParseArguments(arguments)
	expectedInline := []string{"big", "red"}
	if err != nil || !reflect.DeepEqual(inline, expectedInline) || len(named) != 0 {
		t.Errorf("Expected to get big, red, but got inline-  %v, named - %v", inline, named)
	}
	arguments = []string{"big", "red", "--param", "five"}
	inline, named, err = ParseArguments(arguments)
	expectedNamed := make(map[string]string)
	expectedNamed["param"] = "five"
	if err != nil || !reflect.DeepEqual(inline, expectedInline) || !reflect.DeepEqual(named, expectedNamed) {
		t.Errorf("Expected to get big, red, param: five but got inline-  %v, named - %v", inline, named)
	}
	arguments = []string{"big", "red", "--param", "--param1", "five"}
	inline, named, err = ParseArguments(arguments)
	expectedNamed = make(map[string]string)
	expectedNamed["param"] = ""
	expectedNamed["param1"] = "five"
	if err != nil || !reflect.DeepEqual(inline, expectedInline) || !reflect.DeepEqual(named, expectedNamed) {
		t.Errorf("Expected to get big, red, param: '', param1: five but got inline-  %v, named - %v", inline, named)
	}
}

var expectedArgumentsTable = []*ExpectedArgument{
	{Name: "type", Required: true, Type: String, AllowedValues: []string{"new", "create"}, InputType: Argument, Index: 0},
	{Name: "name", Required: true, Type: String, InputType: Named},
	{Name: "controller", Required: false, InputType: Query, DefaultValue: "ControllerName", QueryString: "What is the controller name?"},
}

func TestParseExpectedArguments(t *testing.T) {
	expectedArgumentsText := `
          type* string{new|create} argument
          name* string named
          controller string|ControllerName| query "What is the controller name?"
        `
	expectedArguments, err := ParseExpectedArguments(expectedArgumentsText)
	if err != nil {
		t.Error(err.Error())
	}
	if len(expectedArguments) != 3 {
		t.Errorf("Expected expected arguments length to be 3, but was %v", len(expectedArguments))
	}
	for i, value := range expectedArgumentsTable {
		if !reflect.DeepEqual(expectedArguments[i], value) {
			t.Errorf("Expected expected argument to be %#v, but was %#v", value, expectedArguments[i])
		}
	}
}

func TestDefaultValueValidationForExpectedArguments(t *testing.T) {
	expectedArgumentsText := "type string{new|create}|destroy| argument"
	expectedArguments, err := ParseExpectedArguments(expectedArgumentsText)
	if err == nil {
		t.Errorf("Expected to get 'Default value does not match allowed values' error, but got %#v", expectedArguments[0])
	}
	expectedArgumentsText = "type string{new|create}|create| argument"
	_, err = ParseExpectedArguments(expectedArgumentsText)
	if err != nil {
		t.Errorf("Expected to not get 'Default value does not match allowed values' error, but got %v", err.Error())
	}
}

func TestQuerySuppliedValidationIfQueryArgument(t *testing.T) {
	expectedArgumentsText := "controller string|ControllerName| query"
	expectedArguments, err := ParseExpectedArguments(expectedArgumentsText)
	if err == nil {
		t.Errorf("Expected to get 'Type of expected argument is query, but query string is not supplied' error, but got %#v", expectedArguments[0])
	}
}

func TestRequiredExpectedArgumentsValidation(t *testing.T) {
	expectedArgumentsText := "type* string|defaultValue| argument"
	expectedArguments, err := ParseExpectedArguments(expectedArgumentsText)
	if err == nil {
		t.Errorf("Expected to get 'Required expected arguments should not have default values' error, but got %#v", expectedArguments[0])
	}
}

func TestIntExpectedArgumentAllowedValuesValidation(t *testing.T) {
	expectedArgumentsText := "type* int{2|value} argument"
	expectedArguments, err := ParseExpectedArguments(expectedArgumentsText)
	if err == nil {
		t.Errorf("Expected to get 'Argument type is int, but one of allowed values is non int', but got %#v", expectedArguments[0])
	}
	expectedArgumentsText = "type* int argument"
	expectedArguments, err = ParseExpectedArguments(expectedArgumentsText)
	if err != nil {
		t.Errorf("Expected to get int expected argument with no allowed values, but got %v", err.Error())
	}
}

func TestIntExpectedArgumentDefaultValueValidation(t *testing.T) {
	expectedArgumentsText := "type int|value| argument"
	expectedArguments, err := ParseExpectedArguments(expectedArgumentsText)
	if err == nil {
		t.Errorf("Expected to get 'Argument type is int, but default value is non int', but got %#v", expectedArguments[0])
	}
}

func TestExpectedArgumentSetValueFromArguments(t *testing.T) {
	ea := &ExpectedArgument{Name: "appname", Required: true, Type: String, InputType: Argument, Index: 0}
	named := make(map[string]string)
	err := ea.SetValueFromArguments([]string{"application"}, named)
	if err != nil {
		t.Errorf("Expected value to be set for expected argument, but got error %v", err.Error())
	}
	if ea.Value != "application" {
		t.Errorf("Expected expected argument value to be 'application', but was %v", ea.Value)
	}
	ea.Index = 1
	err = ea.SetValueFromArguments([]string{"application"}, named)
	if err == nil {
		t.Errorf("Expected to get exception while getting argument with index that doesn't exist, but got nil")
	}
	ea.DefaultValue = "defvalue"
	err = ea.SetValueFromArguments([]string{"application"}, named)
	if err != nil {
		t.Errorf("Expected not to get exception as default value is present, but got %v", err.Error())
	}
}

func TestExpecteedArgumentSetValueFromNamed(t *testing.T) {
	ea := &ExpectedArgument{Name: "db", Type: String, InputType: Named}
	named := make(map[string]string)
	named["db"] = "mysql"
	err := ea.SetValueFromArguments([]string{""}, named)
	if err != nil {
		t.Errorf("Expected value  to be set for named argument, but got error %v", err.Error())
	}
	if ea.Value != "mysql" {
		t.Errorf("Expected arguemnt value to be 'mysql', but was %v", ea.Value)
	}
	ea.Name = "db1"
	err = ea.SetValueFromArguments([]string{""}, named)
	if err != nil {
		t.Errorf("Expected not to get error for named argument as it is not required, but got %v", err.Error())
	}
	ea.Required = true
	err = ea.SetValueFromArguments([]string{""}, named)
	if err == nil {
		t.Errorf("Expected to get error while trying to get unexisting named argument, but got nil")
	}
	ea.DefaultValue = "sqlite"
	err = ea.SetValueFromArguments([]string{""}, named)
	if err != nil || ea.Value != "sqlite" {
		t.Errorf("Expected to get sqlite as named argument, but got Value - %v, Error - %v", ea.Value, err.Error())
	}
}

func TestValidateValue(t *testing.T) {
	ea := &ExpectedArgument{Value: "some", Type: Int}
	err := ea.ValidateValue()
	if err == nil {
		t.Errorf("Expected to get value should be int validation error, but got nil")
	}
	ea.Value = "5"
	err = ea.ValidateValue()
	if err != nil {
		t.Errorf("Expected to not get value validation error, but got %v", err.Error())
	}
	ea.AllowedValues = []string{"6", "7"}
	err = ea.ValidateValue()
	if err == nil {
		t.Errorf("Expected to get value not in allowed values validation error, but got nil")
	}
	ea.AllowedValues = []string{"6", "7", "5"}
	err = ea.ValidateValue()
	if err != nil {
		t.Errorf("Expected value to be in allowed values, but got %v", err.Error())
	}
	ea = &ExpectedArgument{Value: "", Type: String}
	err = ea.ValidateValue()
	if err != nil {
		t.Errorf("Expected value to be validated, but got %v", err.Error())
	}
	ea.Required = true
	err = ea.ValidateValue()
	if err == nil {
		t.Errorf("Expected to get 'Argument is required, but was not supplied' error, but got nil")
	}
}

func TestSetValueFromQuery(t *testing.T) {
	input := "value"
	ea := &ExpectedArgument{Type: String, InputType: Query}
	err := ea.SetValueFromQuery(input)
	if err != nil || ea.Value != "value" {
		t.Errorf("Expected to set expected argument value to 'value' from query, but got %v", err.Error())
	}
	input = ""
	err = ea.SetValueFromQuery(input)
	if err != nil || ea.Value != "" {
		t.Errorf("Expected non required query argument not to be set if value is not supplied, but got ", err.Error())
	}
	input = " \n\t\x00value \n\t\x00"
	err = ea.SetValueFromQuery(input)
	if err != nil || ea.Value != "value" {
		t.Errorf("Expected to set expected argument value to 'value' from query, but got %v", err.Error())
	}
	ea = &ExpectedArgument{Type: Int, InputType: Query}
	input = ""
	err = ea.SetValueFromQuery(input)
	if err != nil || ea.Value != "" {
		t.Errorf("Expected non required query argument not to be set if value is not supplied, but got ", err.Error())
	}
}
