// Package for parsing user arguments
package arguments

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// Checks if argument is named argument.
// Named arguments should start with '-' or '--'.
// Example: --js file.js
func isNamedArgument(argument string) bool {
	return strings.HasPrefix(argument, "-") || strings.HasPrefix(argument, "--")
}

// Returns named argument value. Error is returned in case
// argument is not named (is not prefixed with '-' or '--')
func getNamedArgument(argument string) (string, error) {
	if strings.HasPrefix(argument, "--") {
		return argument[2:], nil
	}
	if strings.HasPrefix(argument, "-") {
		return argument[1:], nil
	}
	return "", errors.New("Not a named argument")
}

// Returns parsed arguments. Arguments that were supplied inline
// are returned as string slice (first parameter), named parameters
// are returned as map (second parameter). If any errors happen
// during parsing, error is returned as third parameter.
func ParseArguments(arguments []string) ([]string, map[string]string, error) {
	named := make(map[string]string)
	inline := make([]string, 0)
	for i := 0; i < len(arguments); i++ {
		argument := arguments[i]
		if isNamedArgument(argument) {
			argumentName, err := getNamedArgument(argument)
			if err != nil {
				return nil, nil, err
			}
			named[argumentName] = ""
			if len(arguments) != i+1 {
				nextArgument := arguments[i+1]
				if !isNamedArgument(nextArgument) {
					named[argumentName] = nextArgument
					i++
					continue
				}
			}
		} else {
			inline = append(inline, argument)
		}
	}
	return inline, named, nil
}

// Expected Argument Types
// Argument - parameter fetched from the command line
// Example: geogen js create
// 'create' here stands for Argument type parameter
// Named - parameter specified from the command line
// using '--' or '-' notation
// Example: geogen js --fields 5
// 'fields' here is named parameter with value 5
// Query parameter will be asked after all command
// line arguments are supplied by asking questions
// through the command line and asking for answers
const (
	Argument = iota
	Named
	Query
)

// Types of the Expected Argument field
// Can be String or Int
const (
	String = iota
	Int
)

// Expected Argument struct
type ExpectedArgument struct {
	Name          string
	Required      bool
	Type          int
	AllowedValues []string
	InputType     int
	QueryString   string
	DefaultValue  string
	Index         int
	Value         string
}

// Sets expected argument value from inline and named parameters
// if input type is argument or named. If value could not be retieved,
// error is returned as well
func (self *ExpectedArgument) SetValueFromArguments(inline []string, named map[string]string) error {
	if self.InputType == Query {
		return nil
	}
	self.Value = ""
	var err error
	if self.InputType == Argument {
		self.SetValueForArgumentType(inline)
	}
	if self.InputType == Named {
		self.SetValueForNamedType(named)
	}
	if self.Value == "" && self.DefaultValue != "" {
		self.Value = self.DefaultValue
		err = nil
	}
	err = self.ValidateValue()
	if err != nil {
		return err
	}
	return nil
}

// Sets expected argument value using user input
func (self *ExpectedArgument) SetValueFromQuery(input string) error {
	if self.InputType == Query {
		self.Value = strings.Trim(input, " \n\t\x00")

		if self.Value == "" && self.DefaultValue != "" {
			self.Value = self.DefaultValue
		}
		err := self.ValidateValue()
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("Not a query argument type")
}

// Sets value for argument expected argument type using inline arguments
func (self *ExpectedArgument) SetValueForArgumentType(inline []string) error {
	if self.InputType == Argument {
		if self.Index >= len(inline) {
			return errors.New("No inline argument with specified index found")
		}
		self.Value = inline[self.Index]
		return nil
	}
	return errors.New("Not an argument type")
}

// Sets value for named expected argument type using named arguments map
func (self *ExpectedArgument) SetValueForNamedType(named map[string]string) error {
	if self.InputType == Named {
		value := named[self.Name]
		if value != "" {
			self.Value = value
			return nil
		}
		return errors.New("No named argument with specified name found")
	}
	return errors.New("Not a named type")
}

// Validates expected argument value and returns error
// if validation error occurs
func (self *ExpectedArgument) ValidateValue() error {
	err := self.validateValueInAllowedValues()
	if err == nil {
		err = self.validatePresence()
	}
	if err == nil && self.Value != "" {
		err = self.validateValueInt()
	}
	return err
}

// Validates presence of value if it is required
func (self *ExpectedArgument) validatePresence() error {
	if self.Required && self.Value == "" {
		return errors.New("Argument is required, but was not supplied")
	}
	return nil
}

// Validates that value is int if its type is int and it is not empty
func (self *ExpectedArgument) validateValueInt() error {
	if self.Type == Int {
		_, err := strconv.Atoi(self.Value)
		if err != nil {
			return errors.New("Argument type is int, but value is not int")
		}
	}
	return nil
}

// Validates that value is in one of allowed values
func (self *ExpectedArgument) validateValueInAllowedValues() error {
	if len(self.AllowedValues) > 0 {
		for _, allowedValue := range self.AllowedValues {
			if allowedValue == self.Value {
				return nil
			}
		}
		return errors.New("Value not in allowed values")
	}
	return nil
}

// List of expected argument validations
var validations = []func(ea *ExpectedArgument) error{
	validateDefaultValue,
	validateQuery,
	validateRequired,
	validateIntAllowedValues,
	validateIntDefaultValue,
}

var re *regexp.Regexp = regexp.MustCompile("^\\s*([^\\*]+)(\\*)?\\s+(string|int)({([^}\\s]*)})?\\|?([^\\|\\s]*)\\|?\\s+(query|argument|named)(\\s+\\\"(.*)\\\")?\\s*")

// Parses expected argument line and returns ExpectedArgument struct
func parseExpectedArgument(line string) (*ExpectedArgument, error) {
	matches := re.FindStringSubmatch(line)
	if len(matches) == 0 {
		return nil, errors.New(fmt.Sprintf("Could not parse '%v' as expected argument line", line))
	}
	ea := new(ExpectedArgument)
	ea.Name = matches[1]
	ea.Required = (matches[2] == "*")
	switch matches[3] {
	case "string":
		ea.Type = String
	case "int":
		ea.Type = Int
	}
	if len(matches[5]) > 0 {
		ea.AllowedValues = strings.Split(matches[5], "|")
	}
	ea.DefaultValue = matches[6]
	switch matches[7] {
	case "argument":
		ea.InputType = Argument
	case "named":
		ea.InputType = Named
	case "query":
		ea.InputType = Query
	}
	ea.QueryString = matches[9]
	for _, validation := range validations {
		if err := validation(ea); err != nil {
			return nil, err
		}
	}
	return ea, nil
}

// Default value does not match allowed values validation
func validateDefaultValue(ea *ExpectedArgument) error {
	if ea.AllowedValues != nil && len(ea.AllowedValues) > 0 && ea.DefaultValue != "" {
		for _, allowedValue := range ea.AllowedValues {
			if allowedValue == ea.DefaultValue {
				return nil
			}
		}
		return errors.New("Default value does not match allowed values")
	}
	return nil
}

// Type of expected argument is query, but query string is not supplied validation
func validateQuery(ea *ExpectedArgument) error {
	if ea.InputType == Query && ea.QueryString == "" {
		return errors.New("Type of expected argument is query, but query string is not supplied")
	}
	return nil
}

// Required expected arguments should not have default values validation
func validateRequired(ea *ExpectedArgument) error {
	if ea.Required && ea.DefaultValue != "" {
		return errors.New("Required expected arguments should not have default values")
	}
	return nil
}

// Expected argument type is int, but one of allowed values is not int
func validateIntAllowedValues(ea *ExpectedArgument) error {
	if ea.Type == Int && len(ea.AllowedValues) > 0 {
		for _, value := range ea.AllowedValues {
			_, err := strconv.Atoi(value)
			if err != nil {
				return errors.New("Argument type is int, but one of allowed values is not int")
			}
		}
	}
	return nil
}

// Expected argument type is int, but default value is not int validation
func validateIntDefaultValue(ea *ExpectedArgument) error {
	if ea.Type == Int && ea.DefaultValue != "" {
		_, err := strconv.Atoi(ea.DefaultValue)
		if err != nil {
			return errors.New("Argument type is int, but default value is not int")
		}
	}
	return nil
}

// Parses expectedArguments text and returns expected arguments slice
// along with all the error if there was any.
func ParseExpectedArguments(expectedArgumentsText string) ([]*ExpectedArgument, error) {
	expectedArguments := make([]*ExpectedArgument, 0)
	lines := strings.Split(expectedArgumentsText, "\n")
	inlineArgumentsCount := 0
	for _, line := range lines {
		if strings.Trim(line, " \t\n") != "" {
			expectedArgument, err := parseExpectedArgument(line)
			if err != nil {
				return nil, err
			}
			if expectedArgument.InputType == Argument {
				expectedArgument.Index = inlineArgumentsCount
				inlineArgumentsCount++
			}
			expectedArguments = append(expectedArguments, expectedArgument)
		}
	}
	return expectedArguments, nil
}

// Sets values for all arguments and named expected arguments based on inline and named
// user supplied parameters
func SetUserSuppliedArgumentValues(expectedArguments []*ExpectedArgument, inline []string, named map[string]string) error {
	for _, expectedArgument := range expectedArguments {
		err := expectedArgument.SetValueFromArguments(inline, named)
		if err != nil {
			return err
		}
	}
	return nil
}

// Gets generator parameters from expected arguments and queries all the query expected
// arguments
func GetGeneratorParameters(expectedArguments []*ExpectedArgument) (map[string]string, error) {
	params := make(map[string]string)
	for _, ea := range expectedArguments {
		if ea.InputType == Query {
			for {
				requiredSymbol := ""
				if ea.Required {
					requiredSymbol = "*"
				}
				fmt.Println(ea.QueryString + requiredSymbol)
				input := make([]byte, 200)
				_, err := os.Stdin.Read(input)
				if err != nil {
					return nil, err
				}
				err = ea.SetValueFromQuery(string(input))
				if err == nil {
					break
				}
			}
		}
		if ea.Value != "" {
			params[ea.Name] = ea.Value
		}
	}
	return params, nil
}
