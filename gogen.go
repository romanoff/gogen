// Gogen is generator that can generate files and folders
// based on the template files supplied by the user.
package main

import (
	"bitbucket.org/romanoff/gogen/arguments"
	"bitbucket.org/romanoff/gogen/guard"
	"bitbucket.org/romanoff/gogen/params"
	"bitbucket.org/romanoff/gogen/generator"
	"fmt"
	"os"
)

func main() {
	err := guard.CheckGoGenExists()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	args := os.Args[1:]
	inline, named, err := arguments.ParseArguments(args)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	inline, gogenParams, err := params.GetGogenParams(inline)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	expectedArguments, err := arguments.ParseExpectedArguments(gogenParams.Parameters)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	err = arguments.SetUserSuppliedArgumentValues(expectedArguments, inline, named)
	if err != nil {
		fmt.Println("Usage:")
		fmt.Println(gogenParams.Usage)
		os.Exit(1)
	}
	params, err := arguments.GetGeneratorParameters(expectedArguments)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	generatorContent, err := generator.ParseTemplate(gogenParams.Generator, params)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	files, err := generator.ParseFiles(generatorContent)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	templates, err := generator.CompileTemplates(files, gogenParams.Path)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	err = generator.GenerateFiles(files, templates, params)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
