package params

import "testing"

func TestParseParams(t *testing.T) {
	paramsContent := `
Usage:
Information about usage should go here
Arguments:
appname* string argument
db string|sqlite| named
Validation:
f Rake
d app
Generator:
-> app
some.tmpl -> app/some.js
`

	gogenParameters := `appname* string argument
db string|sqlite| named`
	gogenParams, err := parseParams(paramsContent)
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}
	if gogenParams.Usage != "Information about usage should go here" {
		t.Errorf("Expected usage to be 'Information about usage should go here', but got %v", gogenParams.Usage)
	}
	if gogenParams.Parameters != gogenParameters {
		t.Errorf("Expected to get %v as arguments, but got %v", gogenParameters, gogenParams.Parameters)
	}
	paramsContent = `
Arguments:
appname* string argument
db string|sqlite| named
Validation:
f Rake
d app
Generator:
-> app
some.tmpl -> app/some.js
`
	gogenParams, err = parseParams(paramsContent)
	if err == nil {
		t.Errorf("Expected to get parse params error, but got %#v", gogenParams)
	}
}

func TestRemoveBlank(t *testing.T) {
	content := `
config

`
	content = removeBlank(content)
	if content != "config" {
		t.Errorf("Expected content to be 'config', but got %v", content)
	}
}
