// Package for working with generator parameters
package params

import (
	"bitbucket.org/romanoff/gogen/guard"
	"errors"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

// Gets inline arguments supplied by the user, tries to find generator
// parameters file and if so, returns Gogen params that were retrieved
// from params file and arguments that were not used while searching
// gogen parameters.
// Example:
// gogen template new important
// Finds file ~/.gogen/<repo name>/template/new/params
// And returns ['important'], GogenParams, nil as 'template' and 'new' were
// used while searching params file
func GetGogenParams(arguments []string) ([]string, *GogenParams, error) {
	homePath, err := guard.GetHomePath()
	if err != nil {
		return nil, nil, err
	}
	files, err := ioutil.ReadDir(homePath + "/.gogen")
	if err != nil {
		return nil, nil, err
	}
	templateFolders := make([]string, 0)
	for _, f := range files {
		if f.IsDir() {
			templateFolders = append(templateFolders, f.Name())
		}
	}
	newArguments, paramsDirPath, err := getParamsPath(homePath, templateFolders, arguments)
	if err != nil {
		return nil, nil, err
	}
	paramsContent, err := ioutil.ReadFile(paramsDirPath + "/params")
	if err != nil {
		return nil, nil, err
	}
	gogenParams, err := parseParams(string(paramsContent))
	if err != nil {
		return nil, nil, err
	}
	gogenParams.Path = paramsDirPath
	return newArguments, gogenParams, nil
}

// Returns params file path by looking through all the repositories in .gogen if it
// is present.
func getParamsPath(homePath string, templateFolders []string, arguments []string) ([]string, string, error) {
	for i, _ := range arguments {
		for _, templateFolder := range templateFolders {
			paramsDirPath := homePath + "/.gogen/" + templateFolder
			paramsDirPath += "/" + strings.Join(arguments[:i+1], "/")
			fileInfo, err := os.Stat(paramsDirPath + "/params")
			if err == nil && !fileInfo.IsDir() {
				return arguments[i+1:], paramsDirPath, nil
			}
		}
	}
	return nil, "", errors.New("No params found")
}

// Gogen Params struct
type GogenParams struct {
	Usage      string
	Parameters string
	Validation string
	Generator  string
	Path       string
}

// Parses gogen params content and returns GogenParams struct
func parseParams(paramsContent string) (*GogenParams, error) {
	re, err := regexp.Compile("Usage:((.|\n|\r)*)Arguments:((.|\n|\r)*)Validation:((.|\n|\r)*)Generator:((.|\n|\r)*)")
	if err != nil {
		return nil, err
	}
	matches := re.FindStringSubmatch(paramsContent)
	if len(matches) == 9 {
		gogenParams := new(GogenParams)
		gogenParams.Usage = removeBlank(matches[1])
		gogenParams.Parameters = removeBlank(matches[3])
		gogenParams.Validation = removeBlank(matches[5])
		gogenParams.Generator = removeBlank(matches[7])
		return gogenParams, nil
	}
	return nil, errors.New("Could not parse params content")
}

// Removes all the blank lines from the content string
func removeBlank(content string) string {
	lines := strings.Split(content, "\n")
	nonBlankLines := make([]string, 0)
	for _, line := range lines {
		trimmed := strings.TrimSpace(line)
		if trimmed != "" {
			nonBlankLines = append(nonBlankLines, trimmed)
		}
	}
	return strings.Join(nonBlankLines, "\n")
}
