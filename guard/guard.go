package guard

import (
	"os"
	"os/user"
)

// Returns home folder path for current user
func GetHomePath() (string, error) {
	u, err := user.Current()
	if err != nil {
		return "", err
	}
	return u.HomeDir, nil
}

// Checks for existance of .gogen folder in the home directory
func CheckGoGenExists() error {
	homepath, err := GetHomePath()
	if err != nil {
		return err
	}
	gogenPath := homepath + "/.gogen"
	_, err = os.Stat(gogenPath)
	if err != nil {
		var perm os.FileMode = 0774
		err := os.Mkdir(gogenPath, perm)
		return err
	}
	return nil
}
