package generator

import (
	"reflect"
	"testing"
)

func TestFileIsDir(t *testing.T) {
	f := &File{Path: "app/some"}
	if f.isDir() != true {
		t.Errorf("Expected file without template path to be considered directory, but failed")
	}
	f.TemplatePath = "some.tmpl"
	if f.isDir() != false {
		t.Errorf("Expected file with template path to be considered file, but failed")
	}
}

var expectedFiles = []*File{
	{Path: "app"},
	{TemplatePath: "some.tmpl", Path: "app/some.js"},
}

func TestParseFiles(t *testing.T) {
	generatorContent := `
-> app
some.tmpl -> app/some.js
`
	files, err := ParseFiles(generatorContent)
	if err != nil {
		t.Errorf("Expected to parse generator content, but got %v", err.Error())
	}
	if !reflect.DeepEqual(expectedFiles, files) {
		t.Errorf("Parsed files from generator content don't match")
	}
}

func TestParseFile(t *testing.T) {
	line := "-> app"
	file, err := parseFile(line)
	if err != nil || file.Path != "app" {
		t.Errorf("Expected to get generator.File with 'app' Path, but got %#v", file)
	}
	if file.isDir() != true {
		t.Errorf("Expected to get generator.File directory, but got %#v", file)
	}
	line = "some.tmpl -> app/some.js "
	file, err = parseFile(line)
	if err != nil || file.TemplatePath != "some.tmpl" || file.Path != "app/some.js" {
		t.Errorf("Expected to get generator file with template path - 'some.tmpl', path - 'app/some.js', but got %#v", file)
	}
}
