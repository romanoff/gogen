// Package for generating files
package generator

import (
	"os"
	"errors"
	"regexp"
	"strings"
	"io/ioutil"
	"text/template"
	"bytes"
	"fmt"
	"bitbucket.org/romanoff/gogen/term"
)

// File that will have to be
// created by generator
type File struct {
	TemplatePath string
	Path         string
}

// Returns if file is directory
func (self *File) isDir() bool {
	if self.TemplatePath == "" {
		return true
	}
	return false
}

// Parses all files that will have to be generated
// with generator and returns slice of files or
// error if one occured
func ParseFiles(generatorContent string) ([]*File, error) {
	lines := strings.Split(generatorContent, "\n")
	files := make([]*File, 0)
	for _, line := range lines {
		if len(strings.Trim(line, " \t")) != 0 {
			file, err := parseFile(line)
			if err != nil {
				return nil, err
			}
			files = append(files, file)
		}
	}
	return files, nil
}

var re *regexp.Regexp = regexp.MustCompile("^\\s*(.*)?\\s*->\\s*(.*)\\s*$")

// Parses generator file string and returns *File
// Example: -> folder
//          some.tmpl -> application.js
func parseFile(line string) (*File, error) {
	matches := re.FindStringSubmatch(line)
	if len(matches) == 0 {
		return nil, errors.New("Failed to parse generator file")
	}
	templatePath := strings.TrimSpace(matches[1])
	path := strings.TrimSpace(matches[2])
	return &File{Path: path, TemplatePath: templatePath}, nil
}

func CompileTemplates(files []*File, TemplatesHome string) (map[string]*template.Template, error) {
	templates := make(map[string]*template.Template)
	for _, file := range files {
		if !file.isDir() && templates[file.TemplatePath] == nil {
			path := TemplatesHome + "/" + file.TemplatePath
			content, err := ioutil.ReadFile(path)
			if err != nil {
				return nil, err
			}
			templates[file.TemplatePath], err = template.New(file.TemplatePath).Parse(string(content))
			if err != nil {
				return nil, err
			}
		}
	}
	return templates, nil
}

func GenerateFiles(files []*File, templates map[string]*template.Template, params map[string]string) error {
	var perm os.FileMode = 0774
	for _, file := range files {
		stat, err := os.Stat(file.Path)
		if file.isDir() {
			if err == nil && stat.IsDir() {
				fmt.Printf("     "+term.Bright+term.FgRed+"exists"+term.Reset+"  %v\n", file.Path)
				continue
			}
			err = os.Mkdir(file.Path, perm)
			if err != nil {
				return err
			}
			fmt.Printf("     "+term.Bright+term.FgGreen+"create"+term.Reset+"  %v\n", file.Path)
		}
		if !file.isDir() {
			createFile := true
			if err == nil && !stat.IsDir() {
				for {
					fmt.Printf("     %v file exists. Override? (y/n)\n", file.Path)
					input := make([]byte, 200)
					_, err := os.Stdin.Read(input)
					if err != nil {
						return err
					}
					answer := strings.Trim(string(input), " \n\t\x00")
					if answer == "y" || answer == "Y" {
						break
					}
					if answer == "n" || answer == "N" {
						createFile = false
						break
					}
				}
			}
			if createFile == false {
				break
			}
			template := templates[file.TemplatePath]
			if template == nil {
				return errors.New("Template not found")
			}
			buf := new(bytes.Buffer)
			err = template.Execute(buf, params)
			if err != nil {
				return err
			}
			err = ioutil.WriteFile(file.Path, buf.Bytes(), perm)
			if err != nil {
				return err
			}
			fmt.Printf("     "+term.Bright+term.FgGreen+"create"+term.Reset+"  %v\n", file.Path)
		}
	}
	return nil
}

func ParseTemplate(generatorContent string, params map[string]string) (string, error) {
	tmpl, err := template.New("generator").Parse(generatorContent)
	if err != nil {
		return "", err
	}
	buf := new(bytes.Buffer)
	err = tmpl.Execute(buf, params)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}
